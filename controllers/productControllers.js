const Product = require('../models/productSchema');


//Retrieve all products


module.exports.getAllProducts = () => {

	return Product.find().then(result => {
		if (result.length <= 0) {
			return false
		} else {
			return result
		}
	})
}

// Retrieve a single product
module.exports.getProduct = (reqParams) => {

	return Product.find({"_id": reqParams.productId, "isActive": true}).then(result => {
		if (result.length > 0) {
			return result
		} else {
			return false
		}
	})
}


//Adding product for admin only

module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		SKU: reqBody.SKU
	});


	return Product.find({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		SKU: reqBody.SKU		
	}).then(result => {
		if (result.length > 0) {
			return false
		} else {
			return newProduct.save().then((user, error) => {
				if(error) {
					return {reg: 'Error'}
				} else {
					return true
				}
			})
		}
	})
}


//Updating a product

module.exports.updateProduct = (data) => {
	return Product.findById(data.productId).then(result => {
		if (result === null) {

			return false

		} else {

			result.productName = data.updatedProduct.productName
			result.description = data.updatedProduct.description
			result.price = data.updatedProduct.price
			result.stocks = data.updatedProduct.stocks
			result.SKU = data.updatedProduct.SKU
			

			return result.save().then((result, error) => {
				if (error) {
					return {alert: `Product not updated!`}
				} else {
					return true
				}
			})
			
		}
	})
}


//Archive products
module.exports.toggleArchive = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result.isActive === true) {
			result.isActive = false
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		} else {
			result.isActive = true
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}


//Delete products by ID (Admin) 
module.exports.deleteProduct = (reqParams) => {
	return Product.findByIdAndRemove(reqParams.productId).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//Unarchiving a Product
module.exports.unArchiveProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result === null) {
			return {alert: `No Product!`}
		} else if (result.isActive) {
			return {alert: `Product already active!`}
		} else {
			result.isActive = true

			return result.save().then((result, error) => {
				if (error) {
					return {alert: `Product not updated!`}
				} else {
					return {alert: `${result.name} unarchived!`}
				}
			})
		}
	})
}
