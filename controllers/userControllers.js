const User = require('../models/userSchema');
const Product = require('../models/productSchema');
const bcrypt = require('bcrypt');
const auth = require('../authenticate');



module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)

	})


	return User.find({email: newUser.email}).then(result => {
		if (result.length > 0) {
			return {alert: 'Email already exists'}
		} else {
			return newUser.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		}
	})
}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false
			}
		}
	})
}


//Set user as admin 
module.exports.toggleAdmin = (data) => {

	return User.findById(data.userId).then(result => {
		if (result.isAdmin === true) {
			result.isAdmin = false
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		} else {
			result.isAdmin = true
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}


//get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		if (result === null) {
			return {alert: `Empty users`}
		} else {
			return result
		}   
	})
}



module.exports.addToCart = (data) => {

	let counter = 0;
	let index = 0;

	return Product.findById(data.productId).then(product => {

		return User.findById(data.userId).then(user => {

			for(i = 0; i < user.addedToCart.length; i++) {

				if (user.addedToCart[i].productId !== data.productId) {
					continue
				} else {
					counter++
					index = i
				}
			}

			if (counter > 0) {
				user.addedToCart[index].quantity++

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else 
						return {product: `Product already exists. Quantity Added!`}
				})
			} else {
				user.addedToCart.push({productId: data.productId, productName: product.productName, quantity: 1, price: product.price})

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return {product: `Added to Cart`}
					}
				})		
			}
		})
	})
}


module.exports.removeFromCart = (data) => {

	return User.findById(data.userId).then(user => {

		for(i = 0; i < user.addedToCart.length; i++) {
			if (user.addedToCart[i].productId === data.productId) {
				user.addedToCart.splice(i, 1)

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
		} 
	})
}


module.exports.checkOutOrder = async (data) => {
	
	let productId = [];
	let quantityProductId = [];
	let priceProductId = [];
	let totalPayment = 0;
			 	
	
	let isProductUpdated = false;

	let isUserUpdated = await User.findById(data.userId).then(user => {

		if (user.addedToCart.length > 0) {

			//Containing product ids of carted products
			for (i = 0; i < user.addedToCart.length; i++) {
				productId.push(user.addedToCart[i].productId)
				quantityProductId.push(user.addedToCart[i].quantity)
				priceProductId.push(user.addedToCart[i].price)

				totalPayment += (user.addedToCart[i].quantity * user.addedToCart[i].price)
			}

			let order = {
				userId: data.userId,
				product: user.addedToCart,
				totalPayment: totalPayment,
				modeOfPayment: data.modeOfPayment,
				deliveryAddress: data.deliveryAddress
			}

			user.orders.push(order)

			user.addedToCart = []

			return user.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})

		} else {
			return {alert: `Empty cart`}
		}

	})

	for (i = 0; i < productId.length; i++) {

		let productUpdate = await Product.findById(productId[i]).then(product => {

			let customer = {
				userId: data.userId,
				quantity: quantityProductId[i]
			}


			product.salesQuantity.push(customer)
			product.stocks -= customer.quantity

			if (product.stocks === 0) {
				product.isActive = false
			}
			
			return product.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return {product: `Product Updated!`}
				}
			})
		})

		isProductUpdated = productUpdate
	}

	if (isUserUpdated !== false && isProductUpdated !==false) {
		return true
	} else {
		return false
	}
}

//Get my Orders
module.exports.getMyCart = (userId) => {
	return User.findById(userId).then(result => {
		if (result.addedToCart.length < 0) {
			return {alert: `You have no items in cart`}
		} else { 
			return result.addedToCart
		}
	})
}

module.exports.getUserOrder = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		if (result.orders.length > 0) {
			return result.orders
		} else {
			return false
		}
	})
}


module.exports.getAllOrders = () => {
	return User.find({"isAdmin": false}).then(result => {

		let orderContainer = [];
		let allOrders = [];
		
		if (result.length < 0) {
			return false
		} else {
			orderContainer = result.map(user => user.orders.map(order => allOrders.push(order)))
			return allOrders
		}
	})
} 
