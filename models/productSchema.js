const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({


	productName: {

		type: String,
		required: true
		
	},

	description: {

		type: String,
		required: true
	},

	price: {

		type: Number,
		required: true
	},

	isAvailable: {

		type: Boolean,
		default: true
	},

	stocks: {

		type: Number,
		required: true
	},

	SKU: {

		type: String,
		required: true
	},


	dateAdded: {

		type: Date,
		default: new Date()
	},

	salesQuantity: [
	{

		productId: {

			type: String,
			required: true
		},

		quantity: {
			type: Number,
			required: true
		},

		dateOrdered: {

			type: Date,
			default: new Date()
		},

		totalPayment: {

			type: Number,
			required: [true, "Total Due"]
		}



	}]


});

module.exports = mongoose.model("Products", productSchema);
