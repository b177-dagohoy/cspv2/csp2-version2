const mongoose = require('mongoose');


//User Model
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: true,
		

	},
	lastName: {
		type: String,
		required: true,
		
	},
	email: {
		type: String,
		required: true,
		
	},

	password: {

		type: String,
		required: true
	},

	isAdmin: {

		type: Boolean,
		default: false
	},

	addedToCart: [
	{

		productId:{
			type: String,
			required: true
		},

		productName: {

			type: String,
			required: [true, "Name is required."]
		},

		quantity: {
			type: Number,
			required: true
		},

		price: {
			type: Number,
			required: true
		}


	}],

	orders: [
	{

		product: [
		{

			productId: {
				type: String,
				required: true
			},

			quantity: {
				type: Number,
				required: true
			},

			price: {

				type: Number,
				required: true
			}
		}],

		status: {

			type: String,
			default: "Pending"
		},

		dateOrdered: {

			type: Date,
			default: new Date()
		},

		totalPayment: {

			type: Number,
			required: [true, "Total Due"]
		},

		modeOfPayment: {

			type: String,
			required: [true, "Choose mode of payment"]
		},

		deliveryAddress: {

			street: {
				type: String,
				required: true
			},

			city: {

				type: String,
				required: true
			},

			state: {

				type: String,
				required: true
			},

			country: {

				type: String,
				required: true
			},

			zipcode: {

				type: Number,
				required: true
			}
		}
	}]
});


module.exports = mongoose.model("Users", userSchema);
