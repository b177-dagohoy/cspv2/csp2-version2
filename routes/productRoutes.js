const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../authenticate');


//retrieving all products
router.get('/all', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})

//retrieving a single product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


router.post('/addProduct', auth.verify,  (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
});


//for updating a product
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		updatedProduct: req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
})


router.put('/:productId/archive', auth.verify,  (req, res) => {
	productController.toggleArchive(req.params).then(resultFromController => res.send(resultFromController))
})


router.delete('/:productId', auth.verify,  (req, res) => {
	productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController))
})


//Route for unarchiving a product 
router.put('/:productId/unarchive', auth.verify,  (req, res) => {
	productController.unArchiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})



module.exports = router;
