const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../authenticate');



router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
}); 


router.put('/:userId/setAdmin', auth.verify, (req, res)=> {

	userController.toggleAdmin(req.params).then(resultFromController => res.send(resultFromController))

})


router.get('/', auth.verify, (req, res)=> {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})


router.post('/:productId/cart', auth.verify, auth.isNotAdmin, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
})

router.delete('/:productId/cart', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id
	}

	userController.removeFromCart(data).then(resultFromController => res.send(resultFromController))
})


router.post('/checkout', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		userId: user.id,
		modeOfPayment: req.body.modeOfPayment,
		deliveryAddress: req.body.deliveryAddress
	}

	userController.checkOutOrder(data).then(resultFromController => res.send(resultFromController))
})


router.get('/myCart', auth.verify, auth.isNotAdmin, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getMyCart(userId).then(resultFromController => res.send(resultFromController))
})


router.get('/ordersbyuser/:userId', auth.verify, auth.isAdmin, (req, res) => {
	userController.getUserOrder(req.params).then(resultFromController => res.send(resultFromController))
})


router.get('/orders', auth.verify, auth.isAdmin, (req, res) => {
	userController.getAllOrders().then(resultFromController => res.send(resultFromController))
})


module.exports = router;
