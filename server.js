const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');




const app = express();

dotenv.config({path : './config.env'});
const port = process.env.PORT;

const database = process.env.DATABASE;

mongoose.connect(database, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(() => {
	console.log("Database is now connected.");
}).catch((e)=> {
	console.log(e)
})


//Require Model
const Users = require('./models/userSchema');
const Products = require('./models/productSchema');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));


app.get('/', (req, res) => {

	res.send("Hello World");
})


app.use("/users", userRoutes);
app.use("/products", productRoutes);




app.listen(port, () => {
	console.log(`Server is running at ${process.env.PORT}`)
}) 